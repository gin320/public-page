APPNAME := app

GITCOMMITHASH := $(shell git log --max-count=1 --pretty="format:%h" HEAD)
GITCOMMIT := -X main.revision=$(GITCOMMITHASH)

VERSIONTAG := 1.0.0
VERSION := -X main.version=$(VERSIONTAG)

BUILDTIMEVALUE := $(shell date +%Y-%m-%dT%H:%M:%S%z)
BUILDTIME := -X main.builddate=$(BUILDTIMEVALUE)

LDFLAGS := '-extldflags "-static" -d -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
LDFLAGS_WINDOWS := '-extldflags "-static" -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
all:info clean build

clean:
	rm -rf build

info: 
	@echo - appname:   $(APPNAME)
	@echo - verison:   $(VERSIONTAG)
	@echo - commit:    $(GITCOMMITHASH)
	@echo - buildtime: $(BUILDTIMEVALUE) 


build:
	@echo Building for linux
	@mkdir -p build
	@CGO_ENABLED=0 \
	GOOS=linux \
	go build -o build/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH) -a -ldflags $(LDFLAGS) .
	

update-releasepage:
	mkdir -p public/releases
	cp -v build/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH) public/releases
	sed -i 's/__release_linux__/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH)/g' public/index.html